from random import randint

class MatrixWriter(object):

	def __init__(self, m, n, filler = None):
		"""Creates an m X n matrix of 0s """
		self.m = m
		self.n = n
		self.matrix = [[0]*n for i in range(m)]
		if( filler is not None):
			self.fill(filler)
	#end init
		
		
	def fill(self, filler):
		""" fills a matrix using the function filler
				filler should accept 2 arguments:
					@arg1: row index
					@arg2: column index
					@returns the value of the matrix at that index"""
					
		for i in range(self.m):
			for j in range(self.n):
				self.matrix[i][j] = filler(i,j)
				
	#end filler
	
	def writeToFile(self, File):
		"""File can either be the name of the file or a file"""
		
		if(isinstance(File, file)):
			self._doWrite(File)
		elif(isinstance(File, str)):
			with open(File, 'w') as f:
				self.__doWrite(f)
		
	def __doWrite(self, File):
			File.write(str(self.m) + ' ' + str(self.n) + '\n')
			for row in self.matrix:
				for element in row:
					File.write(str(element) + ' ')
				File.write('\n')
	#end __doWrite
				
	def __str__(self):
		res = ''
		for row in self.matrix:
				for element in row:
					res = res + str(element) + ' '
				res = res + '\n'
		return res
	#end __str__
	

def constantFiller(constant):
	"""always returns a constant"""
	def inner(i,j):
		return constant
	return inner

def randomFiller(minInt, maxInt):
	def inner(i,j):
		return randint(minInt,maxInt)
	return inner
	
