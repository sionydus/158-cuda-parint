from MatrixWriter import MatrixWriter, randomFiller, constantFiller
from random import sample
rf = randomFiller(0,1)


def rowFill(rows, value, rf):
	def inner(i,j):
		if i in rows:
			return value
		elif j == 0: #guarentees the solution is always correct
			return 0
			
		else:
			return rf(i,j);
	return inner

def colFill(cols, value, otherwise):
	def inner(i,j):
		if j in cols:
			return value
		else:
			return otherwise
	return inner

rowfill = rowFill([1,3,4], 1, rf)
colfill = colFill([1,3,4], 1, 0)

#small test
m = MatrixWriter(10,10, rowfill)
s = MatrixWriter(1, 10, colfill)
m.writeToFile('test1.txt')
s.writeToFile('sol1.txt')

#small number of sets, large number of elements
elementsInCommon = sample(range(100000), 17)
m = MatrixWriter(1000000,3, rowFill(elementsInCommon,1,rf))
s = MatrixWriter(1, 1000000, colFill(elementsInCommon,1,0))

m.writeToFile('test2.txt')
s.writeToFile('sol2.txt')

#small number of elements, large number of sets
elementsInCommon = [2]
m = MatrixWriter(3,1000000, rowFill(elementsInCommon,1,rf))
s = MatrixWriter(1, 3, colFill(elementsInCommon,1,0))

m.writeToFile('test3.txt')
s.writeToFile('sol3.txt')

#medium tests 3,333 X 3,287
for i in range (4,10):
	elementsInCommon = sample(range(3287), 17)
	m = MatrixWriter(3333,3287, rowFill(elementsInCommon,1,rf))
	s = MatrixWriter(1, 3333, colFill(elementsInCommon,1,0))

	m.writeToFile('test' + str(i) + '.txt')
	s.writeToFile('sol'  + str(i) + '.txt')
	
#"really big" test, 10,000 X 10,000 of all ones
cf = constantFiller(1)
m = MatrixWriter(10000,10000, cf)
s = MatrixWriter(1, 10000, cf)

m.writeToFile('test10.txt')
s.writeToFile('sol10.txt')





