#ifndef __READMATRIX
#define __READMATRIX
#include <string>
using namespace std; 

int* readMatrix1D(char fileName[], unsigned int* m, unsigned int* n);
int* readMatrix1D(string fileName, unsigned int* m, unsigned int* n);
int** readMatrix2D(char fileName[], unsigned int* m, unsigned int* n);
int** readMatrix2D(string fileName, unsigned int* m, unsigned int* n);
#endif
