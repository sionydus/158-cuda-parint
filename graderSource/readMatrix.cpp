#include <fstream>
#include <cstdlib>
#include "readMatrix.h"

int* readMatrix1D(char fileName[], unsigned int* m, unsigned int* n){
	ifstream file(fileName);//open the file
	if(file.is_open()){
		file >> *m >> *n; //read the dimension of the matrix
		int* mat = new int[(*m)*(*n)];
		//read the file
		for(unsigned int i =0; i < (*m)*(*n) && (file >> mat[i]); ++i);
	
		file.close();//close the file
		return mat;
	}
	return NULL;
	
}


int* readMatrix1D(string fileName, unsigned int* m, unsigned int* n){
	return(readMatrix1D(fileName.c_str(),m,n));
}


int** readMatrix2D(char fileName[], unsigned int* m, unsigned int* n){
	ifstream file(fileName);//open the file
	if(file.is_open()){
		file >> *m >> *n; //read the dimension of the matrix
		int** mat = new int*[*m];
		for(unsigned int i =0; i < *m; ++i){
			mat[i] = new int[*n];
		}
		
		//read the file
		for(unsigned int i =0; i<*m; ++i){
			for(unsigned int j =0; j < *n && (file >> mat[i][j]); ++j);
		}
		
		file.close();//close the file
		return mat;
	}
	return NULL;
	
}

int** readMatrix2D(string fileName, unsigned int* m, unsigned int* n){
	return(readMatrix2D(fileName.c_str(),m,n));
}
