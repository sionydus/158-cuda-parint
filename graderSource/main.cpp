#include <iostream>
#include "readMatrix.h"
#include "cudaparint.h"
#include "getRealTime.cpp"
using namespace std;


bool isCorrect(int* solMatrix, int* studentMatrix, int numElements){
	for(int i = 0; i < numElements; i++){
		if(solMatrix[i] != studentMatrix[i]){
			return false;
		}
	}
	return true;
}

//@arg1: the name of file containing the test matrix
//@arg2: the name of the file containing the solution matrix 
int main(int argc, char* argv[]){
	int* testMatrix;
	int* solutionMatrix;
	int* result; 
	int test_nrows = 0;
	int test_ncolumns = 0;
	int sol_nrows = 0;
	int sol_ncolumns = 0;

	if(argc < 3){ //check to make sure the arguments were called correctly
		cout << "Not enough arguments." << endl;
		cout << "Usage: cudaparint.out NameOfTestMatrix NameOfSolutionMatrix" << endl;
		return 1;
	}

	//read in the matrices
	testMatrix     = readMatrix1D(argv[1], (unsigned int*)&test_nrows, (unsigned int*)&test_ncolumns);
	solutionMatrix = readMatrix1D(argv[2], (unsigned int*)&sol_nrows,  (unsigned int*)&sol_ncolumns);
	result = new int[test_nrows];

	//start timer
	double before = getRealTime();
	//call the student's function to get their result
	parint(testMatrix, test_nrows, test_ncolumns, result);
	double after = getRealTime();
	//end timer
	
	
	//check result
	if(isCorrect(solutionMatrix, result, test_nrows)){
		cout << "TRUE" << endl;
	} else{
		cout << "FALSE" << endl;
	}
	//print time taken
	cout << after - before << endl;

	//clean up
	delete []testMatrix;
	delete []solutionMatrix;
	delete []result;
	
	return 0;
}
