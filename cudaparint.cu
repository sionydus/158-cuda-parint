#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <iostream>
#include <limits.h>
#include <math.h>

#define MAX_NUM_WARPS_PER_SM 8

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) 
	{
		fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

// get the intersect of one row, end index is exclusive
__device__ __host__ int rowIntersect(int *x, int start, int end){
	int intersect = 1;
	for(int i = start; i < end; i ++){
		intersect &= x[i];
	}
	return intersect;
}

// kernel function for each thread
__global__ void threadIntersect(int *x, int nc, int nRPT, int nTWLR, int *rs){
	int myIdx = blockIdx.x * blockDim.x + threadIdx.x; // assuming each block has one thread
	int start, end; // end is exclusive
	
	// avoid thread divergence ??
	int start1, start2;
	start1 = myIdx * (nRPT - 1);
	start2 = (nRPT - 1) * nTWLR + (myIdx - nTWLR) * nRPT;
	start = myIdx < nTWLR ? start1 : start2;
	end = start + nRPT;
	end -= myIdx < nTWLR ? 1 : 0;

	for (int i = start; i < end; i ++){
		rs[i] = rowIntersect(x, i * nc, (i + 1) * nc);
	}
}

void splitIndices ( int nrows, int nThreads , int &nRowPerThread, int &nThreadWith1LessTask)
{
	int chunkSize = nrows / nThreads;
	if(nrows % nThreads != 0)
		chunkSize ++;
	// delta is the number of nodes having one less task
	int delta = chunkSize * nThreads - nrows;

	nRowPerThread = chunkSize;
	nThreadWith1LessTask = delta;
}		/* -----  end of function splitIndices  ----- */

void printChunks(int *chunks, int size){
	for(int i = 0; i < size; i ++){
		printf("%d: %d, %d\n", i, chunks[i * 2], chunks[i * 2 + 1]);
	}
}

int getClosestMultiple(int n, int multiplier){
	int remainder = n % multiplier;
	return n - remainder;
}


void parint(int *x, int nr, int nc, int *rslt)
{
	// if only 1 column, copy x to result and return
	if (nc == 1){
		memcpy(rslt, x, sizeof(int) * nr);
		return;
	}

	unsigned long long workSize;
	workSize = nr * nc;

	// get number of GPUs
	cudaDeviceProp props;
	int devCount;
	cudaGetDeviceCount(&devCount);
	if(devCount <= 0){
		fprintf(stderr, "No GPU is found, exiting.\n");
		exit(1);
	}

	// get properties of GPU and decide number of threads
	cudaGetDeviceProperties(&props, 0);
	unsigned short nSM = props.multiProcessorCount;
	unsigned int warpSize = props.warpSize;
	unsigned int nThreads = min(warpSize * MAX_NUM_WARPS_PER_SM * nSM, getClosestMultiple(nr, nSM));
	//printf("nThread: %d, nSM: %d, nr: %d\n", nThreads, nSM, nr); //TODO remove


	//if (nThreads > 0) {
	if (0) {
		// doing parallel

		int nRPT, nTWLR;
		splitIndices(nr, nThreads, nRPT, nTWLR); // number of rows per thread, number of threads with 1 less row
		//printChunks(chunks, nThreads);

		// initialize and copy arrays into GPU
		int *dx, // device input matrix
				*drs; // device results 

		gpuErrchk(cudaMalloc((void **)&dx, workSize * sizeof(int)));
		gpuErrchk(cudaMemcpy(dx, x, workSize * sizeof(int), cudaMemcpyHostToDevice));

		gpuErrchk(cudaMalloc((void **)&drs, nr * sizeof(int)));

		// call kernel function
		dim3 dimGrid(nSM, 1);
		dim3 dimBlock(nThreads / nSM, 1, 1);
		threadIntersect<<<dimGrid, dimBlock>>>(dx, nc, nRPT, nTWLR, drs);

		// wait for kernel to finish
		gpuErrchk(cudaThreadSynchronize());

		// copy results back to host
		gpuErrchk(cudaMemcpy(rslt, drs, nr * sizeof(int), cudaMemcpyDeviceToHost));

		// clean up
		gpuErrchk(cudaFree(dx));
		gpuErrchk(cudaFree(drs));

	} else {
		// doing serial
		//printf("doing serial\n"); // TODO remove
		for (int i = 0; i < nr; i++) {
			rslt[i] = rowIntersect(x, i * nc, (i + 1) * nc);
		}
	}
}

//#define DEV
#ifdef DEV
int main(int argc, char **argv)
{
	// printf("%d\n", getClosestMultiple(atoi(argv[1]), atoi(argv[2])));
	// return 0;

	int  *rslt;
	int nr, nc;

	// initial values
	int x[16] = {   0, 0, 1, 1, 
		1, 1, 1, 1, 
		1, 1, 0, 1, 
		1, 1, 1, 1};
	nr = 4;
	nc = 4;

	rslt = new int[nr];

	// test code
	parint(x, nr, nc, rslt);

	// print rslt
	for(int i = 0; i < nr; i ++){
		printf("%d ", rslt[i]);
	}
	printf("\n");

	
}
#endif
